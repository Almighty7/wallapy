'''
This is the file where you should place your constants
'''

# section angles
# ANGLE_FULL_CIRCLE = 360
# ANGLE_HALF_CIRCLE = 180
# endsection angles

# section time
TIME_GAME_DURATION = 180
TIME_SLEEP_WAIT_START = 3
TIME_SLEEP_WAIT_SHORT = 0.3
TIME_SLEEP_WAIT_MEDIUM = 1
# endsection time

# section distance
# endsection distance

# section speed
SPEED_DRIVE_FAST = 200
SPEED_DRIVE_MED = 150
SPEED_DRIVE_SLOW = 100

SPEED_TURN_FAST = 150
SPEED_TURN_MED = 100
SPEED_TURN_SLOW = 50
# endsection speed

# section camera
# endsection camera

# section motor
# endsection motor

# section servo
SERVO_CLAW_PORT = 2
SERVO_CLAW_CLOSED = 000  # instead of 300 for security
SERVO_CLAW_SEMI = 900
SERVO_CLAW_OPEN = 1500
SERVO_CLAW_STEPS_DEFAULT = 11
SERVO_LEFT_LIFT_PORT = 0
SERVO_RIGHT_LIFT_PORT = 1
SERVO_LEFT_LIFT_LOWERED = 2000
SERVO_RIGHT_LIFT_LOWERED = 1000
SERVO_LIFT_MIN = 0
SERVO_LIFT_HOLD = 500
SERVO_LIFT_MAX = 1000
# endsection servo

# section other

# endsection other
