Walla.py
===
Walla.py contains python bindings for the libwallaby library.

Automatic installation packages and the Eclipse plugin can be found [here](https://gitlab.com/groups/Almighty7).
